package com.agiledeveloper;

import clojure.lang.LockingTransaction;
import clojure.lang.Ref;

import java.util.HashSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ComputeLinksSTM implements ComputeLinks {
  private ExecutorService executorService;
  private Ref tasksCount = new Ref(0);
  private CountDownLatch latch = new CountDownLatch(1);

  Ref visited = new Ref(new HashSet<String>());

  public long countLinks(String url) {
    executorService = Executors.newFixedThreadPool(128);

    scheduleVisit(url);

    try {
      latch.await(1, TimeUnit.MINUTES);
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      executorService.shutdown();
    }
    return (getDeref()).size();
  }

  private void scheduleVisit(String url) {
    changeTaskCountBy(1);
    executorService.execute(() -> visitLink(url));
  }

  private void visitLink(String url) {
    getDeref().add(url);

    LinksFinder.getLinks(url)
            .stream()
            .filter(link -> !getDeref().contains(link))
            .forEach(this::scheduleVisit);
    changeTaskCountBy(-1);
    if ((int) tasksCount.deref() == 0) latch.countDown();
  }

  private void changeTaskCountBy(int i) {
    try {
      LockingTransaction.runInTransaction(() -> tasksCount.set((int) tasksCount.deref() + i));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private HashSet<String> getDeref() {
    return (HashSet<String>) visited.deref();
  }
}
